# connectserver Script #

This script allows you to easily manage and connect to your SSH, CIFS, RDP or SFTP servers. It is very easy to implement other protocols and since it is published under the GPLv3+ lincese, you are free to do whatever you want with it! If you don't know how to implement other protocols, send me an email and I'll implement that in the next version.

### Author & Project Site
Made by flo@floriantraun.at - Florian Traun

### Installation
1. Download and unzip the source code - [Releases] (https://github.com/floriantraun/connectserver/releases)
2. Move connectserver either to your user bin or to /usr/local/bin with _mv ./bin/connectserver /usr/local/bin/_
3. Make it executeable with _chmod +x /usr/local/bin/connectserver_
4. You can execute the script by typing _connectserver_ from anywhere in the terminal. Please note, that it can be executed by every user and that it creates a seperate config file for every user.

### Usage
```
connectserver [OPTIONS] [SERVERNAME]
```
### Bugs & Issues
If it's a bug, open an Issue; if you have a fix, read [CONTRIBUTING.md] (CONTRIBUTING.md) and open a Pull Request. The todo list is located at the end of that file.

### Want to contribute?
Please read [CONTRIBUTING.md] (CONTRIBUTING.md)

### Options
```
a, --add
	   Adds a server to the local database file
-d, --delete [SERVERNAME]
	   Removes a server from the local database file 
-l, --list
	   Lists all available servers with associated details
-h, --help
	   Displays this help file
-e, --edit [SERVERNAME]
	   Lets you edit server details
```

### Examples
```
connectserver webserver
connectserver --delete customerssh
connectserver --edit
connectserver --edit homeserver
connectserver --add
```

### Todo
* Nothing to do :)

If you have any suggestions, leave me a message or create an issue.

Of course you are welcome to fork this repository and create a commit. If nothings wrong with your changes, I'll merge it with the master tree.

### List of contributors (add yourself if you want to)
* [floriantraun](https://github.com/floriantraun) (project lead)

### License
License GPLv3+: GNU GPL version 3 or later - http://gnu.org/licenses/gpl.html.

This is free software: you are free to change and redistribute it. There is NO WARRANTY, to the extent permitted by law.
