# Contributing to connectserver #
When you're contributing to connectserver, there are a few rules you should follow.  First and foremost, connectserver should be able to (compile and) run on Linux. _Please_ keep your temporary files (such as `.bk`, `.*~`, or even `.tmp`) out of the project.

### How To Contribute
1. Fork The Repository ([floriantraun/connectserver](https://github.com/floriantraun/conenctserver))
2. Branch It
	- this is because when you do the pull request for it, it includes commits you make after you make the pull request and before the pull request is accepted
3. Make Your Changes
4. Commit Your Changes
5. Do 3 and 4 as Needed
6. Push Your Changes Back to GitHub
7. Start a Pull Request on the Repository
	- make sure you explain what the pull request does

### Indentation
With connectserver, you should use two (2) spaces as an indent; tabs should not be used as indentation ever. It gets annoying when you have both in there, and it clogs up commit logs trying to fix it.

### Brackets
Brackets should be on the next line of a function definition or an if directive. Brackets should always be on their own line.

### Issues
Make sure you
- Describe the problem
- Show how to reproduce it, if applicable
- Explain what you think is causing it, if applicable
- Give a plausible solution

### Commits
Commits should be in the present tense, and with Title Capitalization. If needed, a body should be on the next line in normal capitalization.
